# Installieren
## Deskop
### ssh Schlüsssel erstellen

```
ssh-keygen -t rsa -b 4096 
ssh-copy-id -i ~/.ssh/id_rsa.pub user@server 

``` 
### Go Installieren
```

wget https://dl.google.com/go/go1.12.9.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.12.9.linux-amd64.tar.gz
sudo nano /etc/environment
------Datei----------
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"
   ------------------
 reboot

``` 
### Mysql Workbench
##### Installieren

* [Mysql Workbench](https://dev.mysql.com/get/Downloads/MySQLGUITools/mysql-workbench-community_8.0.16-1ubuntu18.04_amd64.deb)

### Installieren wir Visual Studio Code
* [Visual Studio](https://code.visualstudio.com/)

## Server 

### certbot Installieren
*  [Cerbort](https://certbot.eff.org/)

## Server und Deskopp

### MYSQL 8

``` bash
   wget -c https://dev.mysql.com/get/mysql-apt-config_0.8.13-1_all.deb
   apt-get install  gnupg 
   sudo dpkg -i mysql-apt-config*
   sudo apt update
   sudo apt-get install mysql-server
   sudo systemctl enable mysql
   sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf
   -------------------Datei mysqld.cnf --------------
   [mysqld]
   bind-address = 0.0.0.0
   -------------------Datei mysqd ende----------------
   mysql -u root -p
   use mysql;
      update user set host='%' where user='eigene Benutzername';
      update db set host='%' where user='euer_benutzer';
      CREATE DATABASE git;
      CREATE DATABASE webprogrammieren.de;
      exit;
      sudo service mysql restart
```

### Git

```

sudo apt-get install git-core
sudo apt update
sudo apt install git
 
```