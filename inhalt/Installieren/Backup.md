# Backup
## Linux
```
sudo mkdir /backup
sudo chmod 777 -R /backup
cd /backup
ssh root@webprogrammieren.de rm root.zip
ssh root@webprogrammieren.de mysqldump -u root -p --all-databases > /backup/sicherung.sql;
ssh root@webprogrammieren.de zip -r root.zip /root
scp  root@webprogrammieren.de:root.zip /backup
```
## Window
```   
mkdir c:\Backup
cd c:\Backup
ssh root@webprogrammieren.de rm root.zip
ssh root@webprogrammieren.de mysqldump -u root -p --all-databases >  c:\Backup\sicherung.sql;
ssh root@webprogrammieren.de zip -r root.zip /root
scp  root@webprogrammieren.de:root.zip  c:\Backup

```  