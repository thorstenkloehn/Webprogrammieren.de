# Willkommen auf meine Seite

*  [Installieren](/Installieren)

## Frontend
* [HTML](/HTML)
* [CSS](/CSS)
* Javascript

## Backend

* Go (Golang)
* Mysql

## Server Mieten

*  https://www.kimsufi.com/de
*  https://servdiscount.com/
*  https://www.hetzner.de/sb
* https://oneprovider.com