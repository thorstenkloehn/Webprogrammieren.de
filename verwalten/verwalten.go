package verwalten

import (
	"encoding/base64"
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/russross/blackfriday"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"text/template"
)

type Benutzer struct {
	Name   string
	Zufall string
}

func zufallausgebenl() *string {
	b := Benutzer{}
	hochladen, _ := ioutil.ReadFile("Nutzerdaten/benutzer.json")
	json.Unmarshal(hochladen, &b)
	zufallstring := b.Zufall
	return &zufallstring
}

var (
	key   = []byte(*zufallausgebenl())
	store = sessions.NewCookieStore(key)
)

func (b *Benutzer) Anmeldung(w http.ResponseWriter, r *http.Request) {
	webseiten := Webseiten{}
	webseiten.Titel = "Login"
	webseiten.Hosting = os.Args[1] + "://" + r.Host
	vorlagen.ExecuteTemplate(w, "login.html", webseiten)
}

func (b *Benutzer) Registrieren() {

	data := []byte(os.Args[2] + os.Args[3])
	b.Name = base64.StdEncoding.EncodeToString([]byte(data))
	id := uuid.New()
	b.Zufall = id.String()
	ergebnis, _ := json.Marshal(b)
	ioutil.WriteFile("Nutzerdaten/benutzer.json", ergebnis, 0777)
	os.Exit(0)
}

func (b *Benutzer) Anmeldungbearbeiten(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "Webprogrammieren")
	webseiten := Webseiten{}
	hochladen, _ := ioutil.ReadFile("Nutzerdaten/benutzer.json")
	json.Unmarshal(hochladen, &b)
	zurueck, _ := base64.StdEncoding.DecodeString(string(b.Name))
	if string(zurueck) == r.FormValue("Nutzername")+r.FormValue("Passwort") {
		session.Values["authenticated"] = true
		session.Save(r, w)
		http.Redirect(w, r, os.Args[1]+"://"+r.Host, 301)
		return

	} else {
		webseiten.Titel = "Fehler"
		vorlagen.ExecuteTemplate(w, "fehler.html", webseiten)

		return
	}

}

type Webseiten struct {
	WebsiteTitel    string
	Titel           string
	Sidebar         []string
	Seiten          string
	Inhalt          string
	Hosting         string
	Zugriff         bool
	SidebarVorschau string
}

var vorlagen, _ = template.ParseGlob("vorlagen/*")

//Anzeigen
func (d *Webseiten) Anzeigen(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	d.Titel = vars["Titel"]
	d.Seiten = vars["Seiten"]
	d.Hosting = os.Args[1] + "://" + r.Host
	if d.Seiten == "" {

		d.Seiten = "index"
		if d.Titel == "" {
			d.Titel = "home"
		}
	}
	jsonDateieinlesen, _ := ioutil.ReadFile("config.json")
	json.Unmarshal(jsonDateieinlesen, &d)
	sidebardatei, _ := ioutil.ReadFile("inhalt/" + d.Titel + "/Sidebar.txt")
	ausgabe := strings.Split(string(sidebardatei), ",")
	d.Sidebar = ausgabe
	dateiausgabe, _ := ioutil.ReadFile("inhalt/" + d.Titel + "/" + d.Seiten + ".md")
	output := blackfriday.MarkdownCommon(dateiausgabe)
	d.Inhalt = string(output)
	session, _ := store.Get(r, "Webprogrammieren")
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		d.Zugriff = false
		vorlagen.ExecuteTemplate(w, "index.html", d)
		return
	} else {
		d.Zugriff = true
		vorlagen.ExecuteTemplate(w, "index.html", d)
		return
	}

}
func (d *Webseiten) Vorschau(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	d.Titel = vars["Titel"]
	d.Seiten = vars["Seiten"]
	d.Hosting = os.Args[1] + "://" + r.Host

	jsonDateieinlesen, _ := ioutil.ReadFile("config.json")
	json.Unmarshal(jsonDateieinlesen, &d)
	sidebardatei, _ := ioutil.ReadFile("inhalt/" + d.Titel + "/Sidebar.txt")
	d.SidebarVorschau = string(sidebardatei)
	dateiausgabe, _ := ioutil.ReadFile("inhalt/" + d.Titel + "/" + d.Seiten + ".md")

	d.Inhalt = string(dateiausgabe)
	session, _ := store.Get(r, "Webprogrammieren")
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		d.Zugriff = false
		vorlagen.ExecuteTemplate(w, "vorschau.html", d)
		return
	} else {
		d.Zugriff = true
		vorlagen.ExecuteTemplate(w, "vorschau.html", d)
		return
	}

}

func (d *Webseiten) Bearbeiten(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	d.Titel = vars["Titel"]
	os.Mkdir("inhalt/"+d.Titel, 0777)
	d.Seiten = vars["Seiten"]
	d.Hosting = os.Args[1] + "://" + r.Host
	session, _ := store.Get(r, "Webprogrammieren")
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		d.Zugriff = false
		vorlagen.ExecuteTemplate(w, "bearbeiten.html", d)
		return
	} else {
		d.Zugriff = true
		d.SidebarVorschau = r.FormValue("Sidebar")
		d.Inhalt = r.FormValue("Inhalt")
		ioutil.WriteFile("inhalt/"+d.Titel+"/Sidebar.txt", []byte(d.SidebarVorschau), 0777)
		ioutil.WriteFile("inhalt/"+d.Titel+"/"+d.Seiten+".md", []byte(d.Inhalt), 0777)

		// vorlagen.ExecuteTemplate(w, "bearbeiten.html", d)
		http.Redirect(w, r, os.Args[1]+"://"+r.Host+"/"+d.Titel+"/"+d.Seiten, 301)

		cmd := exec.Command("git", "add", "inhalt/"+d.Titel+"/"+d.Seiten+".md")
		cmd.Run()
		cmd1 := exec.Command("git", "add", "inhalt/"+d.Titel+"/Sidebar.txt")
		cmd1.Run()
		cmd2 := exec.Command("git", "commit", "-m", "Automatisch Ergestellt")
		cmd2.Run()
		return
	}
}
