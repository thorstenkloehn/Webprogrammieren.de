package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"os"
	"webprogrammieren.de/Thorsten/Webprogrammieren.de/verwalten"
)

var (
	start    = verwalten.Webseiten{}
	benutzer = verwalten.Benutzer{}
)

func ZugangsdatenDatei() {
	filename := "Nutzerdaten/benutzer.json"
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		fmt.Println("Geben Sie bitte 'webprogrammieren Install Nutzername Passwort' ein ")
		os.Exit(0)
	}
}
func Zugangserstellen() {
	os.Mkdir("Nutzerdaten", 0777)
	if len(os.Args) < 3 {
		fmt.Println("Es fehlen Zugangdaten")
		os.Exit(0)
	} else if len(os.Args) < 4 {
		fmt.Println("Es fehlen Zugangdaten")
		os.Exit(0)
	} else {
		fmt.Println("Zugangsdaten wird gespeichert")
		benutzer.Registrieren()
		os.Exit(0)
	}
}

func main() {


	muxhandler := mux.NewRouter()
	muxhandler.HandleFunc("/login", benutzer.Anmeldung)
	muxhandler.HandleFunc("/bearbeitung/{Titel}/{Seiten}", start.Bearbeiten)
	muxhandler.HandleFunc("/test", benutzer.Anmeldungbearbeiten)
	muxhandler.HandleFunc("/vorschau/{Titel}/{Seiten}", start.Vorschau)
	muxhandler.HandleFunc("/", start.Anzeigen)
	muxhandler.HandleFunc("/{Titel}/{Seiten}", start.Anzeigen)
	muxhandler.HandleFunc("/{Titel}", start.Anzeigen)
	muxhandler.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))

	if len(os.Args) < 2 {
		ZugangsdatenDatei()
		fmt.Println("Geben Sie bitte 'webprogrammieren http oder https' ein")
		os.Exit(0)

	}

	switch os.Args[1] {
	case "Install":
		Zugangserstellen()
	case "http":
		http.ListenAndServe(":3000", muxhandler)
	case "https":
		http.ListenAndServeTLS(":3000", "/etc/letsencrypt/live/webprogrammieren.de/cert.pem", "/etc/letsencrypt/live/webprogrammieren.de/privkey.pem", muxhandler)
	default:
		fmt.Println("Geben Sie bitte 'webprogrammieren http oder https' ein")
	}
}
